import subprocess

def restart_service(service_name):
    print(f"Restarting {service_name}...")
    subprocess.run(['sudo', 'systemctl', 'restart', service_name], check=True)

if __name__ == "__main__":
    service_to_restart = 'example-service'
    restart_service(service_to_restart)
