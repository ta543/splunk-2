import os

def scale_resources(service_name, scale_up=True):
    if scale_up:
        print(f"Scaling up resources for {service_name}...")
    else:
        print(f"Scaling down resources for {service_name}...")

if __name__ == "__main__":
    service_to_scale = 'example-service'
    scale_resources(service_to_scale, scale_up=True)
