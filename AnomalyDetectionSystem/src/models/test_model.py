from sklearn.metrics import accuracy_score

def test_model(model, X_test, y_test):
    predictions = model.predict(X_test)
    accuracy = accuracy_score(y_test, predictions)
    print(f"Model Accuracy: {accuracy}")
    return accuracy

if __name__ == "__main__":
    # Assume 'trained_model', 'X_test', and 'y_test' are defined
    accuracy = test_model(trained_model, X_test, y_test)
