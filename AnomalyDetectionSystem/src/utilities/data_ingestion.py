def ingest_data(source, destination):
    print(f"Ingesting data from {source} to {destination}...")

if __name__ == "__main__":
    data_source = 'log_file.log'
    data_destination = 'Splunk'
    ingest_data(data_source, data_destination)
