import unittest
from src.utilities.data_ingestion import ingest_data
from src.automation.scale_resources import scale_resources
from subprocess import CalledProcessError

class TestEndToEnd(unittest.TestCase):
    def test_data_ingestion(self):
        # Assuming data_ingestion returns True on success
        result = ingest_data('source.log', 'Splunk')
        self.assertTrue(result)

    def test_scale_resources(self):
        # Test to ensure that scaling command executes without errors
        try:
            scale_resources('example-service', True)
            response = True
        except CalledProcessError:
            response = False
        self.assertTrue(response)

if __name__ == '__main__':
    unittest.main()
