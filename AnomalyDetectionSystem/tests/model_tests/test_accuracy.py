import unittest
from src.models.train_model import train_model
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_classification

class TestModelAccuracy(unittest.TestCase):
    def setUp(self):
        # Generate synthetic data
        X, y = make_classification(n_samples=1000, n_features=20, random_state=42)
        X_train, self.X_test, self.y_train, self.y_test = train_test_split(X, y, test_size=0.25, random_state=42)
        self.model = train_model()

    def test_accuracy(self):
        predictions = self.model.predict(self.X_test)
        accuracy = (predictions == self.y_test).mean()
        self.assertGreater(accuracy, 0.80)  # Assuming we expect at least 80% accuracy

if __name__ == '__main__':
    unittest.main()
