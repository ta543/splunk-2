#!/bin/bash

echo "Starting maintenance tasks..."

# Cleanup old log files
echo "Cleaning up old log files..."
find /var/log -name "*.log" -type f -mtime +10 -exec rm -f {} \;

# Docker system maintenance
echo "Performing Docker system maintenance..."
docker system prune -f

# Check system health
echo "Checking system health..."
systemctl status docker | grep "active (running)" > /dev/null
if [ $? -eq 0 ]; then
    echo "Docker is running."
else
    echo "Docker is not running, starting Docker..."
    systemctl start docker
fi

# Optionally, add more system checks and maintenance tasks

echo "Maintenance tasks completed."
