#!/bin/bash

# Ensuring the script is run as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "Updating system packages..."
apt-get update && apt-get upgrade -y

echo "Installing required packages..."
apt-get install -y python3 python3-pip git

echo "Installing Docker..."
apt-get install -y docker.io
systemctl start docker
systemctl enable docker

echo "Installing Kubernetes tools..."
apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl

echo "Installing Python dependencies..."
pip3 install numpy pandas scikit-learn splunk-sdk

echo "All tools and dependencies installed successfully."
