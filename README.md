# 🚀 Automated Anomaly Detection System

## 🌟 Project Overview
This **Automated Anomaly Detection System** leverages Splunk and machine learning to detect and respond to anomalies in infrastructure and application metrics. It is designed to assist DevOps teams in monitoring, analyzing, and reacting to operational issues in real time, thereby enhancing system reliability and performance.

## 🛠 Features
- **Real-Time Data Processing:** Utilizes Splunk for continuous data ingestion and analysis 🔄.
- **Machine Learning Integration:** Employs machine learning models through the Splunk Machine Learning Toolkit (MLTK) to identify anomalies 🔍.
- **Automated Alerts and Incident Response:** Features automated scripts for alerting and incident response, such as resource scaling or service restarts ⚠️.
- **Customizable Dashboards:** Provides Splunk dashboards for an intuitive visualization of metrics and anomalies 📊.

## 📋 Prerequisites
Before setting up the project, make sure the following are installed:
- Docker 🐳
- Kubernetes 🌐
- Python 3.x 🐍
- Splunk (either Enterprise or Cloud) 🔗
- Git (for version control) 📁

## ⚙️ Installation

### Step 1: Clone the Repository
Clone the repository to your local machine using Git:
```bash
git clone https://github.com/yourusername/anomaly-detection-system.git
cd anomaly-detection-system
```

### Step 2: Set Up the Environment
Execute the setup script to install all required tools and dependencies:

```bash
./AnomalyDetectionSystem/tools/setup_tools.sh
```

### Step 3: Install Python Dependencies
Install the necessary Python libraries listed in requirements.txt:


```bash
pip install -r requirements.txt
```

### Step 4: Start Splunk
Ensure that Splunk is running and properly configured. If using Docker, you can run a Splunk instance with the following command:


```bash
docker run -d -p 8000:8000 -p 8089:8089 --name splunk splunk/splunk:latest
```

### Step 5: Configure Data Inputs
Configure the data inputs in Splunk as outlined in config/splunk/inputs.conf.

## 🚀 Usage
### Running the System
Activate the anomaly detection scripts:


```bash
python src/models/train_model.py
python src/automation/scale_resources.py
```

### Viewing Dashboards
Access the Splunk dashboard via http://localhost:8000 to view real-time data and analytics.

## 🤝 Contributing
We welcome contributions! Please consult our Contributing Guidelines for information on how to submit bug fixes, propose features, and submit pull requests.

## 📜 License
This project is licensed under the MIT License - see the LICENSE.md file for details.

